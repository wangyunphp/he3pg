#ifndef PG_STAT_SHARE_STORAGE_H
#define PG_STAT_SHARE_STORAGE_H

#include "catalog/genbki.h"
#include "catalog/pg_stat_share_storage_d.h"
CATALOG(pg_stat_share_storage,4780,StatShareStorageRelationId) BKI_SHARED_RELATION BKI_ROWTYPE_OID(4783,StatShareStorageRelation_Rowtype_Id) BKI_SCHEMA_MACRO
{
	int64       applyvcl;           /* vdl */
#ifdef CATALOG_VARLEN			    /* variable-length fields start here */
	timestamptz checkpointtime;	    /*checkpiont time*/
#endif
}FormData_pg_stat_share_storage;
typedef FormData_pg_stat_share_storage *Form_pg_stat_share_storage;

#endif
