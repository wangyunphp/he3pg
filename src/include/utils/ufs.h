#include <sys/types.h>
#include <unistd.h>
#include<stdint.h>
#include "fs.grpc-c.h"

/*typedef struct {
	size_t len;
	uint8_t* data
} ProtobufCBinaryData;
*/
typedef struct {
	int fd;
        int errNo;
} fdInfo;
extern void ufs_init_client();
//extern void shutdown();
extern fdInfo  he3Open(char *fileName,uint32_t fileFlages,uint16_t fileMode,uint8_t state);
extern uint64_t he3Lseek(uint64_t fd,uint64_t offset,uint64_t whence);
extern int walRead(uint64_t fd,ProtobufCBinaryData buf, uint64_t offset);
extern int he3Write(uint64_t fd, ProtobufCBinaryData buf, uint64_t offset);
extern int walLocalRead(uint64_t fd, ProtobufCBinaryData buf, uint64_t offset);
extern int walRestoreRead(uint64_t fd, ProtobufCBinaryData buf, uint64_t offset);
extern ProtobufCBinaryData  dataRead(uint64_t fd, uint64_t offset, uint64_t lastLsn);
extern int he3Unlink(char* filename);
extern int he3Close(uint64_t fd);
extern int he3Truncate(uint64_t fd, uint64_t offset);
extern int he3Fsync(uint64_t fd);
