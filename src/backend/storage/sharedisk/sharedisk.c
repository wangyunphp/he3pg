#include "storage/sharedisk.h"

static ShareDiskInfo *ShareDiskCtl = NULL;

Size 
ShareDiskShmemSize(void) {
	Size		size = 0;
	size = add_size(size,sizeof(ShareDiskInfo));
	return size;
}

void
ShareDiskShmemInit(void)
{
	bool		foundShareDisk;

	ShareDiskCtl = (ShareDiskInfo *)
		ShmemInitStruct("ShareDisk Ctl", ShareDiskShmemSize(), &foundShareDisk);

	if (foundShareDisk)
	{
		return;
	}
	memset(ShareDiskCtl, 0, sizeof(ShareDiskInfo));
}

